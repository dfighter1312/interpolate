# Interpolate

## How to use

For any type of data tables, select 2 columns: An column which is dependent variable (now called as X) and another column which is independent variable (now called as Y).
Convert the table (in any data types) into the following format:
- pointsToEvaluate (`double[]`) which contains X where Y is null.
- functionValuesX (`double[]`) which contains X where X and Y are both non-null.
- functionValuesY (`double[]`) which contains Y where X and Y are both non-null.

For instance, with the table:
| X | Y    |
|---|------|
| 2 | 3    |
| 3 | null |
| 4 | 6    |
| 5 | 7.5  |
| 6 | null |
| 7 | null |
| 8 | 12   |

Then:
- pointsToEvaluate = [3, 6, 7]
- functionValuesX = [2, 4, 5, 8]
- functionValuesY = [3, 6, 7.5, 12]

Finally, called the program as in the example in `Program.java` or in `interpolate` function at `interp/Interpolate.java`.
