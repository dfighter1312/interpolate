package interp;

import java.util.ArrayList;
import java.util.List;

public class Interpolate {


    public Interpolate() {
    }

    // public List<Double> interpolate(String type, double[] data) {
        // Convert data to suitable form here

        // Decide interpolation method, 'linear' is recommended
        // if (type == "linear") {
        //     return this.evaluateLinear(pointsToEvaluate, functionValuesX, functionValuesY);
        // } else if (type == "exponential") {
        //     return this.evaluateExponential(pointsToEvaluate, functionValuesX, functionValuesY);
        // } else if (type == "polynomial") {
        //     return this.evaluatePolynomial(pointsToEvaluate, functionValuesX, functionValuesY);
        // } else if (type == "step") {
        //     return this.evaluateStep(pointsToEvaluate, functionValuesX, functionValuesY, useRightBorder);
        // } else if (type == "value") {
        //     return this.evaluateValue(pointsToEvaluate, functionValuesX, functionValuesY, fillValue);
        // }
        // return null;
    // }

    public List<Double> evaluateLinear (double[] pointsToEvaluate, double[] functionValuesX, double[] functionValuesY) throws Exception {
        if (functionValuesX.length != functionValuesY.length) {
            throw new Exception("The lengths of array X and Y must be identical");
        }
        List<Double> results = new ArrayList<>();
        for (int i = 0; i < pointsToEvaluate.length; i++) {
            double point = pointsToEvaluate[i];
            int index = this.findIntervalBorderIndex(point, functionValuesX, true);
            if (index == functionValuesX.length - 1) {
                index--;
            }
            double interpolateValue = this.linearInterpolation(
                point,
                functionValuesX[index],
                functionValuesY[index],
                functionValuesX[index + 1],
                functionValuesY[index + 1]
            );
            results.add(interpolateValue);
        }
        return results;
    }

    public List<Double> evaluateExponential (double[] pointsToEvaluate, double[] functionValuesX, double[] functionValuesY) throws Exception {
        if (functionValuesX.length != functionValuesY.length) {
            throw new Exception("The lengths of array X and Y must be identical");
        }
        List<Double> results = new ArrayList<>();
        for (int i = 0; i < pointsToEvaluate.length; i++) {
            double point = pointsToEvaluate[i];
            int index = this.findIntervalBorderIndex(point, functionValuesX, true);
            if (index == functionValuesX.length - 1) {
                index--;
            }
            double interpolateValue = this.exponentialInterpolation(
                point,
                functionValuesX[index],
                functionValuesY[index],
                functionValuesX[index + 1],
                functionValuesY[index + 1]
            );
            results.add(interpolateValue);
        }
        return results;
    }

    public List<Double> evaluatePolynomial (double[] pointsToEvaluate, double[] functionValuesX, double[] functionValuesY) throws Exception {
        if (functionValuesX.length != functionValuesY.length) {
            throw new Exception("The lengths of array X and Y must be identical");
        }
        List<Double> results = new ArrayList<>();
        for (int i = 0; i < pointsToEvaluate.length; i++) {
            double point = pointsToEvaluate[i];
            double interpolateValue = this.nevillesIteratedInterpolation(
                point,
                functionValuesX,
                functionValuesY
            );
            results.add(interpolateValue);
        }
        return results;
    }

    public List<Double> evaluateStep (double[] pointsToEvaluate, double[] functionValuesX, double[] functionValuesY, boolean useRightBorder) throws Exception {
        if (functionValuesX.length != functionValuesY.length) {
            throw new Exception("The lengths of array X and Y must be identical");
        }
        List<Double> results = new ArrayList<>();
        for (int i = 0; i < pointsToEvaluate.length; i++) {
            double point = pointsToEvaluate[i];
            results.add(functionValuesY[this.findIntervalBorderIndex(point, functionValuesX, useRightBorder)]);
        }
        return results;
    }

    public List<Double> evaluateValue (double[] pointsToEvaluate, double[] functionValuesX, double[] functionValuesY, double fillValue) {
        List<Double> results = new ArrayList<>();
        for (int i = 0; i < pointsToEvaluate.length; i++) {
            double point = pointsToEvaluate[i];
            int intervalBorderLeft = this.findIntervalBorderIndex(point, functionValuesX, true);
            results.add(functionValuesX[intervalBorderLeft] == point ? functionValuesY[intervalBorderLeft] : fillValue);
        }
        return results;
    }
    
    private int findIntervalBorderIndex (double point, double[] intervals, boolean useRightBorder) {
        
        // If point is beyond given intervals
        if (point < intervals[0]) {
            return 0;
        }
        if (point > intervals[intervals.length - 1]) {
            return intervals.length - 1;
        }

        // If point is inside interval
        // Start searching on a full range of intervals
        int indexOfNumberToCompare;
        int leftBorderIndex = 0;
        int rightBorderIndex = intervals.length - 1;

        while (rightBorderIndex - leftBorderIndex != 1) {
            indexOfNumberToCompare = leftBorderIndex + (int) Math.floor((rightBorderIndex - leftBorderIndex) / 2);
            if (point >= intervals[indexOfNumberToCompare]) {
                leftBorderIndex = indexOfNumberToCompare;
            } else {
                rightBorderIndex = indexOfNumberToCompare;
            }
        }
        return useRightBorder ? rightBorderIndex : leftBorderIndex;
    }

    private double linearInterpolation (double x, double x0, double y0, double x1, double y1) {
        double a = (y1 - y0) / (x1 - x0);
        double b = - a * x0 + y0;
        return a * x + b;
    }

    private double exponentialInterpolation (double x, double x0, double y0, double x1, double y1) {
        return y0 * (Math.pow(y1 / y0, (x - x0) / (x1 - x0)));
    }

    private double nevillesIteratedInterpolation (double x, double[] X, double[] Y) {
        List<List<Double>> Q = new ArrayList<>();
        List<Double> Y_temp = new ArrayList<>();
        for (double ele: Y) {
            Y_temp.add(ele);
        }
        Q.add(Y_temp);
        int i = 1;
        int j = 1;
        for (i = 1; i < X.length; i++) {
            List<Double> initArr = new ArrayList<>();
            for (int k = 0; k < X.length; k++) {
                initArr.add(0.0);
            }
            Q.add(initArr);
            for (j = 1; j <= i; j++) {
                Q.get(j).set(
                    i,
                    ((x - X[i-j]) * Q.get(j-1).get(i) - (x - X[i]) * Q.get(j-1).get(i-1)) / (X[i] - X[i-j])
                );
            }
        }
        return Q.get(j-1).get(i-1);
    }
}