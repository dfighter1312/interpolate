import java.util.List;

import interp.Interpolate;

class Program {

    private static Interpolate interpolate = new Interpolate();
    
    public static void main(String[] args) throws Exception {
        double[] points = new double[]{0.021, 0.022};
        double[] functionValuesX = new double[]{0, 1.1, 2.4};
        double[] functionValuesY = new double[]{12, 431, 39};

        List<Double> linearPoints = interpolate.evaluateLinear(points, functionValuesX, functionValuesY);
        for (int i = 0; i < linearPoints.size(); i++) {
            System.out.println(linearPoints.get(i));
        }
    }
    
}
